define(['custom'], function(custom) {

	
	var fileName  = 'home';
  	custom.logger(fileName + "Controller: Init");
	  return {
	  	///////////////////////////////////////
	    apply: function(app) {
			custom.logger(fileName + "Controller: Loaded");
				app.controller(fileName + 'Controller', function($scope, $timeout, cfpLoadingBar, $firebase, psResponsive) {	   
				    
					// INIT
					$scope.init = function(){
						 cfpLoadingBar.start();
					};	
					
					$timeout(function(){
						 cfpLoadingBar.complete();
					}, 1000);
				
					$scope.lorem = "Lorem ipsum dolor sit amet, an epicuri mediocrem vituperata eos. Illud volumus periculis per no, cu torquatos definitionem eum. Est cu hendrerit vituperatoribus. In officiis nominati eum, aperiri dolorem usu ut. In meliore denique suavitate vim.";
					
					
						/* SETUP */
			            function addSlide(target, style) {
			                var i = target.length;
			                target.push({
			                	id: (i + 1),
			                    label: 'Slide: ' + (i + 1),
			                    img: 'http://lorempixel.com/450/300/' + style + '/' + (i % 10) 
			                });
			            };
			
			            function addSlides(target, style, qty) {
			                for (var i=0; i < qty; i++) {
			                    addSlide(target, style);
			                }
			            }
						/* END SETUP */
			
			
						/* STANDARD */ 
						$scope.standard = [];
				        addSlides($scope.standard, 'nightlife', 6);
						$scope.standardIndex = 0; 				
						/* END */
                                
            
	    
				});				
	    },
	    ///////////////////////////////////////
  };
});
